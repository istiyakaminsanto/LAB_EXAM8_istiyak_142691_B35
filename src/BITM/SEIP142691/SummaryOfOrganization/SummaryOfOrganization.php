<?php
namespace App\SummaryOfOrganization;
use PDO;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class SummaryOfOrganization extends DB{
    public $id="";
    public $summary_of_organization="";
    public $author_name="";

    public function __construct(){
        parent:: __construct();
        if(!isset($_SESSION)) session_start();
    }

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('summary_of_organization',$postVariableData)){
            $this->summary_of_organization = $postVariableData['summary_of_organization'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
    }



    public function store(){

        $arrData = array( $this->summary_of_organization, $this->author_name);

        $sql = "Insert INTO summary_of_organization(summary_of_organization, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<h3>Success! Data Has Been Inserted Successfully :)</h3>");
        else
            Message::message("<h3>Failed! Data Has Not Been Inserted Successfully :( </h3>");

        Utility::redirect('create.php');


    }// end of store method



    public function index()
    {
        try{
            $query = "SELECT * FROM summary_of_organization WHERE is_deleted='No'";
            $STMT = $this->DBH->query($query);
            $data = $STMT->fetchAll();

            return $data;
        }catch (\PDOException $e){
            echo $e->getMessage();
        }
    }

    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from summary_of_organization where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }


    public function update(){

        $arrData = array ($this->summary_of_organization, $this->author_name);
        $sql = "UPDATE summary_of_organization SET summary_of_organization = ?, author_name = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }



    public function delete(){

        $sql = "Delete from summary_of_organization where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }




    public function softDelete(){

        $sql = "Update summary_of_organization SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }


    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from summary_of_organization where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }


    public function recover(){

        $sql = "Update summary_of_organization SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }




}

?>


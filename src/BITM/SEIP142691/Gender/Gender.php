<?php
namespace App\Gender;
use PDO;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Gender extends DB{
    public $id="";
    public $gender="";
    public $author_name="";

    public function __construct(){
        parent:: __construct();
        if(!isset($_SESSION)) session_start();
    }

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('gender',$postVariableData)){
            $this->gender = $postVariableData['gender'];
        }

        if(array_key_exists('author_name',$postVariableData)){
            $this->author_name = $postVariableData['author_name'];
        }
    }



    public function store(){

        $arrData = array( $this->gender, $this->author_name);

        $sql = "Insert INTO gender(gender, author_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("<h3>Success! Data Has Been Inserted Successfully :)</h3>");
        else
            Message::message("<h3>Failed! Data Has Not Been Inserted Successfully :( </h3>");

        Utility::redirect('create.php');


    }// end of store method







    public function index()
    {
        try{
            $query = "SELECT * FROM gender WHERE is_deleted='No'";
            $STMT = $this->DBH->query($query);
            $data = $STMT->fetchAll();

            return $data;
        }catch (\PDOException $e){
            echo $e->getMessage();
        }
    }
    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from gender where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }


    public function update(){

        $arrData = array ($this->gender, $this->author_name);
        $sql = "UPDATE gender SET gender = ?, author_name = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }



    public function delete(){

        $sql = "Delete from gender where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }




    public function softDelete(){

        $sql = "Update gender SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }


    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from gender where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }


    public function recover(){

        $sql = "Update gender SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }






}

?>


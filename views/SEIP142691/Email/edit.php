<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

use App\Email\Email;

$objBookTitle = new Email();
$objBookTitle->setData($_GET);
$oneData= $objBookTitle->view("obj");



?>




<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <header>
            <div class="logo_area">
                <p class="logo">Atomic Project</p>
                <h3 class="subLogo">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="../BookTitle/index.php">BOOKTITLE</a></li>
                    <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li class="active"><a href="../Email/index.php">EMAIL</a></li>
                    <li><a href="../Hobbies/index.php">HOBBIES</a></li>
                    <li><a href="../City/index.php">CITY</a></li>
                    <li><a href="../ProfilePicture/index.php">PROFILE PICTURE</a></li>
                    <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>
                </ul>

            </nav>
        </header>
        <div><img src="../../../resource/img/email.jpg" alt=""></div>
        <p class="title">Email</p>

         <fieldset class="control-group ">
            <legend>Add your Email Address</legend>
            <form action="update.php" method="post" class="form-inline">

                <input type="hidden" name="id" value="<?php echo $oneData->id ?>">

                <div class="input_form">
                    <label for="InputEmail">Email Name &nbsp;</label>
                    <input type="email" id="InputEmail" class="input-xxlarge" value="<?php echo $oneData->email; ?>" name="email">
                </div>
                <div class="input_form">
                    <label for="Author">User Name</label>
                    <input type="text" id="Author" class="input-xxlarge" value="<?php echo $oneData->author_name ?>" name="author_name">
                </div>
                <div>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    <input type="submit" class="btn btn-success" value="submit">
                </div>
            </form>
        </fieldset>


    </div>
    <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
</div>
</body>
</html>
<?php
require_once("../../../vendor/autoload.php");
use App\City\City;

$objBookTitle = new City();

$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view("obj");
?>



<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <header>
            <div class="logo_area">
                <p class="logo">Atomic Project</p>
                <h3 class="subLogo">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="../BookTitle/index.php">BOOKTITLE</a></li>
                    <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li><a href="../Email/index.php">EMAIL</a></li>
                    <li><a href="../Hobbies/index.php">HOBBIES</a></li>
                    <li class="active"><a href="../City/index.php">CITY</a></li>
                    <li><a href="../ProfilePicture/index.php">PROFILE PICTURE</a></li>
                    <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>
                </ul>

            </nav>
        </header>
        <div><img src="../../../resource/img/city.jpg" alt=""></div>
        <p class="title">City</p>

        
        <div class="container-fluid">
            <a href="../City/index.php"><button class='btn btn-primary'>All City List</button></a>
        </div>
        <div class="container-fluid">
            <div class="">
                <table border="2px" class="table table-bordered">
                    <th>ID</th>
                    <th>BookTitle</th>
                    <th>User Name</th>

                    <tr>
                        <td><?php echo "$oneData->id"; ?></td>
                        <td><?php echo "$oneData->city"; ?></td>
                        <td><?php echo "$oneData->author_name"; ?></td>

                    </tr>
                </table>
            </div>
        </div>


    </div>
    <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
</div>
</body>
</html>


<table border="2px">

</table>



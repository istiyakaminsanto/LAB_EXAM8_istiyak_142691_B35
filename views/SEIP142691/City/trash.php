<?php
require_once("../../../vendor/autoload.php");

use App\City\City;
use App\Message\Message;

$objCity = new City();
$allData = $objCity->trashed();
$serial=1;
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <script type="text/javascript">
        function confirmation() {
            var answer = confirm("Are you sure want to delete")
            if (answer){
                alert("You successfully deleted")
            }
            else{
                alert("You don't delete")
            }
        }

        function recover() {
            alert("You successfully recover from the trash list.if you want to see this  go to main list")
        }


    </script>

</head>
<body>
<div class="container">
    <div class="container-fluid">
        <header>
            <div class="logo_area">
                <p class="logo">Atomic Project</p>
                <h3 class="subLogo">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="../BookTitle/index.php">BOOKTITLE</a></li>
                    <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li><a href="../Email/index.php">EMAIL</a></li>
                    <li><a href="../Hobbies/index.php">HOBBIES</a></li>
                    <li class="active"><a href="../City/index.php">CITY</a></li>
                    <li><a href="../ProfilePicture/index.php">PROFILE PICTURE</a></li>
                    <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>
                </ul>

            </nav>
        </header>
        <div><img src="../../../resource/img/city.jpg" alt=""></div>
        <p class="title">City</p>
    </div>
    <div class="container-fluid">
        <a href="../City/index.php">
            <button class='btn btn-primary'>
                <i class="fa fa-list" aria-hidden="true"></i> All City List
            </button>
        </a>
    </div>

    
    <div class="container-fluid">
        <div class="table-responsive">
            <table border="2px" class="table table-bordered">
                <th>serial</th>
                <th>ID</th>
                <th>City</th>
                <th>Author Name</th>
                <th>Action</th>
                <?php foreach ($allData as $oneData){?>
                    <tr>
                        <td><?php echo $serial++; ?></td>
                        <td><?php echo $oneData['id']; ?></td>
                        <td><?php echo $oneData['city']; ?></td>
                        <td><?php echo $oneData['author_name']; ?></td>
                        <td>
                            <a href='views.php?id=<?php echo $oneData["id"] ?>'>
                                <button class='btn btn-info'>
                                    <i class="fa fa-search-plus" aria-hidden="true"></i> View
                                </button>
                            </a>
                            <a href='edit.php?id=<?php echo $oneData["id"] ?>'>
                                <button class='btn btn-primary'>
                                    <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                                </button>
                            </a>
                            <a href='recover.php?id=<?php echo $oneData["id"] ?>'>
                                <button class='btn btn-warning' onclick="recover()">
                                    <i class="fa fa-refresh" aria-hidden="true"></i> Recover
                                </button>
                            </a>
                            <a href='delete.php?id=<?php echo $oneData["id"] ?>'>
                                <button class='btn btn-danger' onclick="confirmation()">
                                    <i class="fa fa-trash" aria-hidden="true"></i> Delete
                                </button>
                            </a>
                        </td>
                    </tr>
                <?php } ?>

            </table>
        </div>
    </div>




    <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>

</div>
</body>
</html>

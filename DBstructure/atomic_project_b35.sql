-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2016 at 08:47 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b35`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `birthday`, `author_name`, `is_deleted`) VALUES
(1, '2016-11-27', 'Istiyak Amin2', 'No'),
(2, '2016-11-21', 'Pranto', 'No'),
(3, '2016-10-31', 'Pranto', 'No'),
(5, '2016-11-20', 'snatoo', 'No'),
(6, '0000-00-00', 'amin', 'No'),
(8, '0000-00-00', 'amin', 'No'),
(11, '0000-00-00', '222', '2016-11-18 23:04:36'),
(12, '0000-00-00', 'Istiyak_Amin', '2016-11-18 23:21:35'),
(14, '1999-12-03', 'Istiyak Amin', '2016-11-18 23:21:43'),
(16, '2016-11-13', 'Masum Howlader', '2016-11-18 23:21:44'),
(17, '0000-00-00', 'Prantom', '2016-11-18 23:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(1, 'Rono', 'Amin', '2016-11-18 20:49:43'),
(4, 'English 2nd Paper', 'Dr.Anamul HoqQ', 'No'),
(6, 'gitanjali', 'ravindranath thagore', 'No'),
(7, 'ICT', 'Kajol Rani', '2016-11-18 20:49:47'),
(8, 'Bangla 1st paper', 'Imrul kayes', '2016-11-18 22:38:23'),
(9, 'Glasstree', 'stefen ', '2016-11-18 23:22:39'),
(10, 'web desing2', 'md rasel2', 'No'),
(11, 'Sesh prohor', 'Shukanto vottacharjo', 'No'),
(12, 'Business Math', 'Moklechur Rahman', 'No'),
(13, 'Business Math', 'Moklechur Rahman', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `author_name`, `is_deleted`) VALUES
(1, 'Chittagong', 'Istiyak Amin', 'No'),
(2, 'Chittagong', 'Pranto', 'No'),
(4, 'Khulna', 'Hridoy', '2016-11-19 00:28:18'),
(5, 'Barisal', 'bulls', 'No'),
(6, 'Rangpur', 'Riders', '2016-11-19 00:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `author_name`, `is_deleted`) VALUES
(1, 'istiyakaminsanto@gmail.com', 'Istiyak Amin', 'No'),
(2, 'ctgnazrulislam@gmail.com', 'Md.Nazrul Islam', '2016-11-19 01:46:21'),
(3, 'arafatulislamshifat22@gmail.com', 'Arafatul Khan', 'No'),
(5, 'rabeya@gmail.com', 'Rabeya Akter', '2016-11-19 00:06:58'),
(6, 'arafatulislamshifat@gmail.com', 'Arafatul khan', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender`, `author_name`, `is_deleted`) VALUES
(4, 'Male', 'Istiyak Amin', 'No'),
(6, 'male', 'Moslem Uddin', '2016-11-19 00:31:10'),
(7, 'male', 'ravindranath thagore', 'yes'),
(8, 'male', 'Arafatul islam', '2016-11-19 01:46:27'),
(9, 'female', 'Farjana Hafsa', '2016-11-19 00:31:12'),
(10, 'female', 'Naznin Akter', 'No'),
(11, 'female', 'Tania Akter', '2016-11-19 00:35:38'),
(12, 'female', 'Roksana Begum', '2016-11-19 00:35:41'),
(14, 'Female', 'Roksana Begum40', '2016-11-19 00:35:45'),
(15, 'Male', 'Roksana Begum2', 'No'),
(16, 'male', 'Istiyak Amin', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(200) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobbies`, `author_name`, `is_deleted`) VALUES
(1, 'programming,fishing,hacking', 'Istiyak Amin', '2016-11-19 01:45:32'),
(2, 'fishing,hacking,gardening,racing,playing', 'ravindranath thagore', 'No'),
(5, 'fishing,gardening', 'md rasel', 'No'),
(6, 'programming,fishing,hacking,gardening,racing,playing', 'Roksana Begum', 'No'),
(7, 'hacking,gardening,racing,playing', 'ravindranath thagore', '2016-11-19 01:45:39'),
(8, 'programming,fishing,hacking,gardening', 'md rasel2', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(200) NOT NULL,
  `profile_picture` varchar(230) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `profile_picture`, `author_name`, `is_deleted`) VALUES
(1, 'gender.jpg', 'Amin', 'No'),
(2, '1.PNG', 'Istiyak Amin', '2016-11-19 01:45:01'),
(4, '15037165_10154687456737418_94401482866073393_n.jpg', 'pi', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(200) NOT NULL,
  `summary_of_organization` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `summary_of_organization`, `author_name`, `is_deleted`) VALUES
(5, 'this site is very simple', 'Aminul Islam2', '2016-11-19 01:44:48'),
(6, 'this is very bad', 'Emon Ali', 'No'),
(7, 'this is summary', 'Imran Ahmed', 'No'),
(8, 'it''s a summary of organization', 'Md.Nazrul Islam', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
